class Persona:
    def __init__(self,nom,cognom,edat):
        self.nom = nom
        self.cognom = cognom
        self.edat = edat

    def aconseguirDades(self):
        print("El nom i cognom es: "+self.nom +" "+self.cognom+" i la edat es: "+str(self.edat))

p1 = Persona("Sonia", "Blade", 20)
p1.aconseguirDades()
